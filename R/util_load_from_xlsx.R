#' load pia database tables from an xlsx file (that as been created on database export)
#'
#' @param filename the file name of the xlsx file
#'
#' @return the list of loaded tables
#' @author Jörg Henke
#' 
#' @importFrom openxlsx loadWorkbook readWorkbook
#'
#' @examples
#' \dontrun{
#'   f <- paste("/home/henkej/Schreibtisch/nextcloud_rz",
#'              "5230.project_pia/PIA Pretest Example Data",
#'              "Combined.xlsx", sep = "/")
#'   l <- util_load_from_xlsx(f)
#'   l$answers
#' }
util_load_from_xlsx <- function(filename) {
  workbook <- loadWorkbook(file = filename)
  result <- lapply(workbook$sheet_names, function(s){
    readWorkbook(filename, sheet = s)
  })
  names(result) <- workbook$sheet_names
  return(result)
}