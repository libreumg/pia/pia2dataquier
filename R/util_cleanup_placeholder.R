#' cleanup a dataframe column on its placeholder by a replacement
#' 
#' @param fieldname the name of the column in df that is to be handled
#' @param df the data frame to use
#' @param naplaceholder the NA placeholder character that is used as pattern
#' @param replacement the new replacement value
#'
#' @return the cleaned df
#' @author Arne Thode
#' 
util_cleanup_placeholder <- function(fieldname, df, naplaceholder = ".", replacement = NA){
  df$replacementcolumn <- replacement
  con <- df[[fieldname]] != naplaceholder
  df[con,]$replacementcolumn <- df[con,][[fieldname]]
  df[[fieldname]] <- df$replacementcolumn
  df$replacementcolumn <- NULL 
  return(df)
}
